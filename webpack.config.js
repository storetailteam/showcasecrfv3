"use strict";
var settings = require("./src/settings.json"),
    webpack = require("webpack"),
    path = require("path"),
    publicPath = settings.live ? settings.livePath : settings.devPath,
    filename = "live.js",
    files = require('file-system'),
    templatePlugin = new webpack.DefinePlugin({
        "__RETAILER__": JSON.stringify(settings.retailer),
        "__GUTTERS__": JSON.stringify(settings.gutters_template),
        "__CONTAINERBD__": JSON.stringify(settings.color_container),
        "__REALPATH__": publicPath
    }),
    snippet = `__sto.deliv({"placements":{"${settings.format}": {"script": "${publicPath}${filename}"}}})`;
    console.log(templatePlugin);

var sto_loader = path.join(__dirname, "src/sto_loader.js");

files.writeFile("dist/snippet.js", snippet);

module.exports = {
    "entry": "./src",
    "output": {
        "path": __dirname + "/dist",
        "filename": filename,
        "publicPath": publicPath
    },
    performance: {
        hints: process.env.NODE_ENV === 'production' ? "warning" : false
    },
    "module": {
        rules: [
            {
                test: /\.css$/,
                use: [{
                    loader: "style-loader/useable"
                },
                {
                    loader: "css-loader"
                },
                {
                    loader: sto_loader
                }]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.html$/,
                use: [{
                    loader: "html-loader"
                },
                {
                    loader: sto_loader
                }]
            },
            {
                test: /\.(png|jpg|gif|jpeg|pdf|mp4)$/,
                use: [{
                    loader: "file-loader"
                }]
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2|pdf)$/,
                use: [{
                    loader: "file-loader?name=/[name].[ext]"
                }]
            },
            {
                test: /\.scss$/,
                use: [{
                    loader: "style-loader/useable!css-loader!sass-loader"
                }]
            }
        ]
    },
    "devServer": {
        "headers": {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
          "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
        }
    },
    "plugins": [
        templatePlugin
    ]
};

"use strict";

const ctxt_pos = 0,
    ctxt_title = 1,
    ctxt_modal = 2,
    ctxt_prodlist = 3;

var sto = window.__sto,
    settings = require("./../../settings"),
    style = require("./main.css"),
    helper_methods = sto.utils.retailerMethod,
    custom = settings.custom,
    productss = [],
    classParent = "storetail-id-" + settings.format + settings.creaid,

    cta_position = settings.cta_position == '' ? 'bottom' : settings.cta_position,
    cta_target = settings.cta_target == '' ? '_blank' : settings.cta_target,
    cta_size = settings.cta_size != '' ? settings.cta_size : 'small',
    option = settings.option,

    cta2_position = settings.cta2_position == '' ? 'bottom' : settings.cta2_position,
    cta2_target = settings.cta2_target == '' ? '_blank' : settings.cta2_target,
    cta2_size = settings.cta2_size != '' ? settings.cta2_size : 'small',
    option2 = settings.option2,

    isViewFired = false,
    isIMPfired = false,
    isCTAFired = false,
    format = settings.format;

var fontRoboto = document.createElement('link');
fontRoboto.setAttribute('rel', 'stylesheet');
fontRoboto.setAttribute('href', 'https://fonts.googleapis.com/css?family=Roboto');
var head = document.querySelector('head');
head.appendChild(fontRoboto);

custom.sort(function(a, b) {
    return a[ctxt_pos] > b[ctxt_pos]
});

// Get all product ids from custom product organization
custom.forEach(function(e) {
    e[ctxt_prodlist].forEach(function(p) {
        productss.push(p);
    })
});

module.exports = {
    init: _init_(),
}

var settings_Format = {
        "ids": productss,
        "type": settings.format,
        "creaId": settings.creaid
    },
    format_setting = [{
        "id": "0000000000",
        "type": "partner-multi-products",
        "partner": {
            "name": "storetail",
            "crea_id": settings.creaid,
            "crea_type": settings.format,
            "need_products": true,
            "class": "storetail sto-banner storetail-sr-vignette storetail-id-" + settings.format + settings.creaid,
            "position_reference": null,
            "label": "showcase"
        },
        "products": settings.custom.map(function(pdt, i) {
            return {
                "id": pdt[3][0],
                "type": "partner-product",
                "partner": {
                    "name": "storetail",
                    "crea_id": settings.creaid,
                    "crea_type": settings.format,
                    "class": "storetail storetail-sr-vignette storetail-id-" + settings.format + settings.creaid,
                    "position_reference": null,
                }
            }
        })
    }];


function _init_() {

    try {

        var loadData = __sto.getTrackers("SC").length === undefined ? "SC" : ["SC", settings.creaid];

        sto.load(loadData, function(tracker) {

            var removers = {};
            removers[format] = function() {
                style.unuse();
                document.querySelector("." + classParent).remove();
            };


            /*if (window.matchMedia("(max-width: 767px)").matches) {
                tracker.error({
                    "tl": "mobileViewport"
                });
                return removers;
            }*/

            helper_methods.crawlAPI(settings_Format).promise.then(function(result) {
                var avail = checkAvailability(result, settings.custom),
                    ret = avail[1],
                    relevantsProducts = avail[0];
                if (typeof ret === "undefined") {
                    if (relevantsProducts[0].length < 3 && (document.querySelector('.product-grid').clientWidth / document.querySelector('.product-grid>li').clientWidth) < (relevantsProducts[0].length + 1) ) {
                        tracker.availability({
                            "tn": relevantsProducts[0].length
                        });
                        return false;
                    }
                    helper_methods.createFormat(tracker, format_setting, result, 20);
                } else {
                    if (ret === true) {
                        if (relevantsProducts[0].length < 3) {
                            tracker.availability({
                                "tn": relevantsProducts[0].length
                            });
                            return false;
                        }
                        helper_methods.createFormat(tracker, format_setting, result, 20, false);
                    } else if (ret === false) {
                        helper_methods.createFormat(tracker, format_setting, result, 20, true);
                        tracker.error({
                            "tl": "mandatoryNotFound"
                        });
                        return removers;
                    }
                }

                // Outside of onFormatAddedInDOM to prevent multiple onResize event after new format append into DOM

                document.addEventListener("onFormatAddedInDOM", function(e) {
                    try {
                        window.addEventListener("resize", function() {
                            var scContainer = document.querySelector("." + classParent);
                            if (scContainer) {
                                widthProductBox(scContainer);
                                dataCount(scContainer);
                            }
                        });

                        var scNodeContainer = e && e["detail"] && e["detail"]["formatNode"] ? e["detail"]["formatNode"] : null;

                        if (scNodeContainer && scNodeContainer.className.indexOf(classParent) > -1) {
                            style.use();
                            generateVignette(scNodeContainer);
                            widthProductBox(scNodeContainer);
                            dataCount(scNodeContainer);
                            document.querySelector("." + classParent).setAttribute('data-prod-count', document.querySelector("." + classParent).querySelectorAll('.sto-products-container>article').length);



                            if (scNodeContainer.style.display != "none" && isIMPfired === false) {
                                tracker.display();
                                tc_events_20(null, "event", {
                                    "event": "print_item",
                                    "zone": "product-list",
                                    "partenaire": "storetail",
                                    "adtype": "SC",
                                    "creation": settings.creaid,
                                    "campagne": settings.oid,
                                    "category": "storetail",
                                    "action": "print-item",
                                    "label": "showcase"
                                });
                                isIMPfired = true;
                            }

                            if (isCTAFired === false) {
                                optionsLink(scNodeContainer, tracker);
                                isCTAFired = true;
                            }



                            if (isViewFired === false) {
                                __sto.utils.addViewTracking("." + classParent, tracker);
                                var int = window.setInterval(function() {
                                    if (isElementInViewport(scNodeContainer)) {
                                        tc_events_20(null, "event", {
                                            "event": "view_item",
                                            "zone": "product-list",
                                            "partenaire": "storetail",
                                            "adtype": "SC",
                                            "creation": settings.creaid,
                                            "campagne": settings.oid,
                                            "category": "storetail",
                                            "action": "view-item",
                                            "label": "showcase"
                                        });
                                        window.clearInterval(int);
                                    }
                                }, 200);
                                isViewFired = true;
                            }
                        }
                    } catch (e) {
                        console.log(e);
                    }

                });
            });
        });
    } catch (e) {
        console.log(e);
    }
}


function optionsLink(scNodeContainer, tracker) {
    let ctaCount = 0;
    if (option != "none" && option != "") {
        ctaCount++;
        var target = scNodeContainer.querySelector('.sto-vignette .sto-inner-wrapper-vignette');
        var cta = document.createElement('span');
        cta.className = 'sto-cta';
        cta.setAttribute("option1", "");
        cta.setAttribute("cta-nb", ctaCount);

        switch (option) {
            case 'redirection':
                cta.innerHTML = '<div class="sto-options-cta" cta-size=' + cta_size + '><button class="sto-cta-text">' + settings.cta_text + '</button></div>';
                target.appendChild(cta);

                scNodeContainer.querySelector('.sto-cta[option1]').addEventListener('click', function() {
                    tracker.click({
                        "tz": "cta1"
                    });
                    window.open(settings.cta_redirect, cta_target);
                });

                break;
            case 'legal':
                cta.innerHTML = '<div class="sto-options-cta" cta-size=' + cta_size + '><span class="sto-cta-text">' + settings.cta_text + '</span></div>';
                target.appendChild(cta);

                scNodeContainer.querySelector('.sto-cta[option1]').setAttribute('legal', '')
                break;
            case 'file':
                cta.innerHTML = '<div class="sto-options-cta" cta-size=' + cta_size + '><button class="sto-cta-text">' + settings.cta_text + '</button></div>';
                target.appendChild(cta);

                scNodeContainer.querySelector('.sto-cta[option1]').addEventListener('click', function() {
                    tracker.openPDF({
                        "tz": "cta1"
                    });
                    var pdf = require("../../img/" + settings.cta_file);
                    window.open(pdf, "_blank");
                });
                break;
            case 'video':
                cta.innerHTML = '<div class="sto-options-cta" cta-size=' + cta_size + '><button class="sto-cta-text">' + settings.cta_text + '</button></div>';
                target.appendChild(cta);

                scNodeContainer.querySelector('.sto-cta[option1]').addEventListener('click', function() {
                    tracker.playVideo({
                        "tl": "open",
                        "tz": "cta1"
                    })
                    var videoBackground = document.createElement('div');
                    videoBackground.className = 'sto-ecran';
                    var videoBox = document.createElement('div');
                    videoBox.className = 'sto-video';
                    videoBox.innerHTML = '<div class="sto-close"></div>' + settings.cta_embed_video + '</div>';
                    document.querySelector('body').appendChild(videoBackground);
                    document.querySelector('body').appendChild(videoBox);
                    document.querySelector('.sto-close').addEventListener('click', function() {
                        tracker.close({
                            "tl": "closeBtn",
                            "tz": "cta1"
                        });
                        var elemVideo1 = document.querySelector(".sto-ecran");
                        elemVideo1.parentNode.removeChild(elemVideo1);
                        var elemVideo2 = document.querySelector(".sto-video");
                        elemVideo2.parentNode.removeChild(elemVideo2);
                    });
                    document.querySelector('.sto-ecran').addEventListener('click', function() {
                        tracker.close({
                            "tl": "blackScreen",
                            "tz": "cta1"
                        });
                        var elemVideo1 = document.querySelector(".sto-ecran");
                        elemVideo1.parentNode.removeChild(elemVideo1);
                        var elemVideo2 = document.querySelector(".sto-video");
                        elemVideo2.parentNode.removeChild(elemVideo2);
                    });
                });
                break;
            default:
        }
    }

    if (option2 != "none" && option2 != "") {
        ctaCount++;
        var target2 = scNodeContainer.querySelector('.sto-vignette .sto-inner-wrapper-vignette');
        var cta2 = document.createElement('span');
        cta2.className = 'sto-cta';
        cta2.setAttribute("option2", "");
        cta2.setAttribute("cta-nb", ctaCount);

        switch (option2) {
            case 'redirection':
                cta2.innerHTML = '<div class="sto-options-cta" cta-size=' + cta2_size + '><button class="sto-cta-text">' + settings.cta2_text + '</button></div>';
                target2.appendChild(cta2);

                scNodeContainer.querySelector('.sto-cta[option2]').addEventListener('click', function() {
                    tracker.click({
                        "tz": "cta2"
                    });
                    window.open(settings.cta2_redirect, cta2_target);
                });
                break;
            case 'legal':
                cta2.innerHTML = '<div class="sto-options-cta" cta-size=' + cta2_size + '><span class="sto-cta-text">' + settings.cta2_text + '</span></div>';
                target2.appendChild(cta2);

                scNodeContainer.querySelector('.sto-cta[option2]').setAttribute('legal', '')
                break;
            case 'file':
                cta2.innerHTML = '<div class="sto-options-cta" cta-size=' + cta2_size + '><button class="sto-cta-text">' + settings.cta2_text + '</button></div>';
                target2.appendChild(cta2);

                scNodeContainer.querySelector('.sto-cta[option2]').addEventListener('click', function() {
                    tracker.openPDF({
                        "tz": "cta2"
                    });
                    var pdf2 = require("../../img/" + settings.cta2_file);
                    window.open(pdf2, "_blank");
                });
                /*scNodeContainer.querySelector('.sto-logo .sto-cta-img').addEventListener('click', function () {
                  tracker.openPDF();
                  var pdf2 = require("../../img/" + settings.cta2_file);
                  window.open(pdf2, "_blank");
                });*/
                break;
            case 'video':
                cta2.innerHTML = '<div class="sto-options-cta" cta-size=' + cta2_size + '><button class="sto-cta-text">' + settings.cta2_text + '</button></div>';
                target2.appendChild(cta2);

                scNodeContainer.querySelector('.sto-cta[option2]').addEventListener('click', function() {
                    tracker.playVideo({
                        "tl": "open",
                        "tz": "cta2"
                    })
                    var videoBackground2 = document.createElement('div');
                    videoBackground2.className = 'sto-ecran';
                    var videoBox2 = document.createElement('div');
                    videoBox2.className = 'sto-video';
                    videoBox2.innerHTML = '<div class="sto-close"></div>' + settings.cta2_embed_video + '</div>';
                    document.querySelector('body').appendChild(videoBackground2);
                    document.querySelector('body').appendChild(videoBox2);
                    document.querySelector('.sto-close').addEventListener('click', function() {
                        tracker.close({
                            "tl": "closeBtn",
                            "tz": "cta2"
                        });
                        var elemVideo1 = document.querySelector(".sto-ecran");
                        elemVideo1.parentNode.removeChild(elemVideo1);
                        var elemVideo2 = document.querySelector(".sto-video");
                        elemVideo2.parentNode.removeChild(elemVideo2);
                    });
                    document.querySelector('.sto-ecran').addEventListener('click', function() {
                        tracker.close({
                            "tl": "blackScreen",
                            "tz": "cta2"
                        });
                        var elemVideo1 = document.querySelector(".sto-ecran");
                        elemVideo1.parentNode.removeChild(elemVideo1);
                        var elemVideo2 = document.querySelector(".sto-video");
                        elemVideo2.parentNode.removeChild(elemVideo2);
                    });
                });
                break;
            default:
        }
    }

}

function generateVignette(scNodeContainer) {
    var productsContainer = scNodeContainer.querySelector(".product-card").parentElement;
    productsContainer.className = "sto-products-container";

    if (!scNodeContainer.querySelector(".sto-vignette")) {
        var vignette = document.createElement('div');
        vignette.className = "sto-vignette";
        vignette.innerHTML = "<div class='sto-inner-wrapper-vignette'></div>"
        scNodeContainer.prepend(vignette);
    }
}

function widthProductBox(scNodeContainer) {
    /*var getWidthProduct = document.querySelectorAll('.grid .grid-item')[0].getBoundingClientRect().width,
      formatProductCard = scNodeContainer.querySelectorAll('.sto-products-container .product-card'),
      formatVignette = scNodeContainer.querySelector('.sto-vignette');

    formatVignette.style.width = getWidthProduct + "px";
    for (var i = 0; i < formatProductCard.length; i++) {
      formatProductCard[i].style.width = getWidthProduct + "px";
    }*/
}

function dataCount(scNodeContainer) {

    if (window.matchMedia("(max-width: 598px)").matches) {
        scNodeContainer.setAttribute('data-count', 0);
        scNodeContainer.setAttribute('data-viewport', 'mobile');
    } else if (window.matchMedia("(max-width: 767px)").matches) {
        scNodeContainer.setAttribute('data-count', 1);
        scNodeContainer.setAttribute('data-viewport', 'desktop');
        if(document.querySelector("." + classParent).querySelectorAll('.sto-products-container>article').length <1){
            scNodeContainer.style.display = "none";
        }else{
            scNodeContainer.style.display = "list-item";
        };
    } else if (window.matchMedia("(max-width: 1279.5px)").matches) {
        scNodeContainer.setAttribute('data-count', 3);
        if(document.querySelector("." + classParent).querySelectorAll('.sto-products-container>article').length <2){
            scNodeContainer.style.display = "none";
        }else{
            scNodeContainer.style.display = "list-item";
        };
    } else {
        scNodeContainer.setAttribute('data-count', 4);
            if(document.querySelector("." + classParent).querySelectorAll('.sto-products-container>article').length <3){
                scNodeContainer.style.display = "none";
            }else{
                scNodeContainer.style.display = "list-item";
            };
    }
}

function checkAvailability(crawledProds, products) {
    var realProducts = [],
        temporary = [
            [],
            [],
            []
        ],
        rowModal = 0,
        toggleModal = true,
        modalMode = false;

    products.forEach(function(thisProductLine, i) {
        var toggleRowProduct = true;
        thisProductLine[3].forEach(function(thisProduct, j) {
            if (toggleRowProduct === true && Object.keys(crawledProds).includes(thisProduct)) {
                rowModal++;
                toggleRowProduct = false;
                temporary[0].push(thisProduct);
                temporary[1].push(thisProductLine[1]);
                temporary[2].push(thisProductLine[0]);
            }
        });
        if (thisProductLine[2] == true) {
            modalMode = true;
            if (rowModal <= 0) {
                toggleModal = false;
            }
        }
        rowModal = 0;
    });
    realProducts[0] = temporary[0];
    realProducts[1] = temporary[1];
    realProducts[2] = temporary[2];
    return (modalMode == true ? toggleModal == false ? [realProducts, false] : [realProducts, true] : [realProducts])
}

function isElementInViewport(el) {
    if (typeof window.jQuery === "function" && el instanceof window.jQuery) {
        el = el[0];
    }
    var rect = el.getBoundingClientRect();
    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        !(rect.top == rect.bottom || rect.left == rect.right) &&
        !(rect.height == 0 || rect.width == 0) &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
        rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
    );
}
